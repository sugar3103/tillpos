export const DiscountKind = {
  item: 'discountItem',
  price: 'discountPrice',
};

export const ProductID = {
  SMALL: '1',
  MEDIUM: '2',
  LARGE: '3',
};

export const ProductPrice = {
  smallPizza: 11.99,
  mediumPizza: 15.99,
  largePizza: 21.99,
};

export const ProductKind = {
  [ProductID.SMALL]: {
    id: ProductID.SMALL,
    image: '/assets/Pizza-10.jpg',
    description: '10" pizza for one person',
    price: ProductPrice.smallPizza,
    quantity: 0,
    total: 0,
  },
  [ProductID.MEDIUM]: {
    id: ProductID.MEDIUM,
    image: '/assets/Pizza-12.jpg',
    description: '12" pizza for one person',
    price: ProductPrice.mediumPizza,
    quantity: 0,
    total: 0,
  },
  [ProductID.LARGE]: {
    id: ProductID.LARGE,
    image: '/assets/Pizza-15.jpg',
    description: '15" pizza for one person',
    price: ProductPrice.largePizza,
    quantity: 0,
    total: 0,
  },
};

export const ProductsList = [
  {
    id: ProductID.SMALL,
    image: '/assets/Pizza-10.jpg',
    description: '10" pizza for one person',
    price: ProductPrice.smallPizza,
    quantity: 0,
    total: 0,
  },
  {
    id: ProductID.MEDIUM,
    image: '/assets/Pizza-12.jpg',
    description: '12" pizza for one person',
    price: ProductPrice.mediumPizza,
    quantity: 0,
    total: 0,
  },
  {
    id: ProductID.LARGE,
    image: '/assets/Pizza-15.jpg',
    description: '15" pizza for one person',
    price: ProductPrice.largePizza,
    quantity: 0,
    total: 0,
  },
];
