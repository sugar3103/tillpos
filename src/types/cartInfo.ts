interface CartInfo {
  id: string;
  image: string;
  description: string;
  price: number;
  quantity: number;
  total: number;
}

export default CartInfo;
