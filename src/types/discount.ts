interface DiscountType {
  productID: string;
  kind: string;
  quantity?: number;
  price?: number;
}

export default DiscountType;
