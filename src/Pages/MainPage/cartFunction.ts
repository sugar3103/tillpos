import CartInfo from "../../types/cartInfo";
import DiscountType from "../../types/discount";
import { DiscountKind } from "../../Utils/constant";

const addToCart = (cartInfo: CartInfo[], itemToAdd: CartInfo, discount?: DiscountType): CartInfo[] => {
  const filteredProduct = cartInfo.find((eF: CartInfo) => eF.id === itemToAdd.id);

  if (discount?.kind === DiscountKind.item) {
    if (filteredProduct) {
      const newCartInfo = cartInfo.map((e) => {
        const discountQuantity = discount.quantity || 0;
        const quantityToAdd = e.quantity + itemToAdd.quantity < 0 ? 0 : e.quantity + itemToAdd.quantity;
        let itemForFree = 0;
        // free 1 item for each pack of x items
        for (let i = 1; i <= quantityToAdd; i++) {
          if (i > discountQuantity && i % discountQuantity === 1) {
            itemForFree += 1;
          }
        }

        return e.id === discount.productID
          ? {
              ...e,
              quantity: quantityToAdd,
              total: e.price * (quantityToAdd - itemForFree),
            }
          : { ...e };
      });

      return newCartInfo;
    } else if (!filteredProduct) {
      const newAddedCartInfo = [...cartInfo, { ...itemToAdd, quantity: 1, total: itemToAdd.price }];
      return newAddedCartInfo;
    }
  }

  if (discount?.kind === DiscountKind.price) {
    if (filteredProduct) {
      const newCartInfo = cartInfo.map((e) => {
        const discountPrice = discount.price || 0;
        const quantityToAdd = e.quantity + itemToAdd.quantity < 0 ? 0 : e.quantity + itemToAdd.quantity;
        return e.id === discount.productID
          ? {
              ...e,
              price: discountPrice,
              quantity: quantityToAdd,
              total: quantityToAdd * discountPrice,
            }
          : { ...e };
      });
      return newCartInfo;
    } else if (!filteredProduct) {
      const discountPrice = discount.price || itemToAdd.price;
      const newCartInfo = [...cartInfo, { ...itemToAdd, price: discountPrice, total: discountPrice }];

      return newCartInfo;
    }
  }

  if (!discount) {
    if (filteredProduct) {
      const newCartInfo = cartInfo.map((e) => {
        const quantityToAdd = e.quantity + itemToAdd.quantity < 0 ? 0 : e.quantity + itemToAdd.quantity;
        return e.id === itemToAdd.id ? { ...e, quantity: quantityToAdd, total: e.price * quantityToAdd } : { ...e };
      });
      return newCartInfo;
    } else if (!filteredProduct) {
      const newAddedCartInfo = [...cartInfo, { ...itemToAdd, quantity: 1, total: itemToAdd.price }];
      return newAddedCartInfo;
    }
  }

  const newAddedCartInfo = [...cartInfo, { ...itemToAdd, quantity: 1, total: itemToAdd.price }];
  return newAddedCartInfo;
};

const storeCart = (cart: CartInfo[]) => {
  const stringifyCartInfo = JSON.stringify(cart);
  localStorage.setItem("cartInfo", stringifyCartInfo);
};

export { addToCart, storeCart };
