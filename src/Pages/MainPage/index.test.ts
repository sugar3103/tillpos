import CartInfo from "../../types/cartInfo";
import DiscountType from "../../types/discount";
import { DiscountKind, ProductID, ProductKind } from "../../Utils/constant";
import { addToCart } from "./cartFunction";

const smallPizza = ProductKind[ProductID.SMALL];
const mediumPizza = ProductKind[ProductID.MEDIUM];
const largePizza = ProductKind[ProductID.LARGE];

test("normal buy", () => {
  let normalCart: CartInfo[] = [];
  normalCart = addToCart(normalCart, { ...smallPizza, quantity: 1 });
  normalCart = addToCart(normalCart, { ...mediumPizza, quantity: 1 });
  normalCart = addToCart(normalCart, { ...largePizza, quantity: 1 });

  const expectedTotal = smallPizza.price + mediumPizza.price + largePizza.price;
  const totalCart = normalCart.map((e) => e.total).reduce((a, b) => a + b, 0);
  expect(totalCart).toBe(expectedTotal);
});

test("normal buy verything x3", () => {
  let normalCart: CartInfo[] = [];
  normalCart = addToCart(normalCart, { ...smallPizza, quantity: 1 });
  normalCart = addToCart(normalCart, { ...smallPizza, quantity: 2 });
  normalCart = addToCart(normalCart, { ...mediumPizza, quantity: 1 });
  normalCart = addToCart(normalCart, { ...mediumPizza, quantity: 2 });
  normalCart = addToCart(normalCart, { ...largePizza, quantity: 1 });
  normalCart = addToCart(normalCart, { ...largePizza, quantity: 2 });

  const expectedTotal = smallPizza.price * 3 + mediumPizza.price * 3 + largePizza.price * 3;
  const totalCart = normalCart.map((e) => e.total).reduce((a, b) => a + b, 0);
  expect(totalCart).toBe(expectedTotal);
});

test("microsoft buy", () => {
  let microsoftCart: CartInfo[] = [];
  const microsoftDiscount: DiscountType = {
    kind: DiscountKind.item,
    productID: smallPizza.id,
    quantity: 2,
  };
  microsoftCart = addToCart(microsoftCart, { ...smallPizza, quantity: 1 }, { ...microsoftDiscount });
  microsoftCart = addToCart(microsoftCart, { ...smallPizza, quantity: 2 }, { ...microsoftDiscount });
  microsoftCart = addToCart(microsoftCart, { ...largePizza, quantity: 1 });

  const expectedTotal = smallPizza.price * (3 - 1) + largePizza.price;
  const totalCart = microsoftCart.map((e) => e.total).reduce((a, b) => a + b, 0);
  expect(totalCart).toBe(expectedTotal);
});

test("microsoft buy 6 small + 1 large", () => {
  let microsoftCart: CartInfo[] = [];
  const microsoftDiscount: DiscountType = {
    kind: DiscountKind.item,
    productID: smallPizza.id,
    quantity: 2,
  };
  microsoftCart = addToCart(microsoftCart, { ...smallPizza, quantity: 1 }, { ...microsoftDiscount });
  microsoftCart = addToCart(microsoftCart, { ...smallPizza, quantity: 5 }, { ...microsoftDiscount });
  microsoftCart = addToCart(microsoftCart, { ...largePizza, quantity: 1 });

  const expectedTotal = smallPizza.price * (6 - 2) + largePizza.price;
  const totalCart = microsoftCart.map((e) => e.total).reduce((a, b) => a + b, 0);
  expect(totalCart).toBe(expectedTotal);
});

test("amazon buy", () => {
  let amazonCart: CartInfo[] = [];
  const amazonDiscount: DiscountType = {
    kind: DiscountKind.price,
    productID: largePizza.id,
    price: 19.99,
  };
  amazonCart = addToCart(amazonCart, { ...mediumPizza, quantity: 1 });
  amazonCart = addToCart(amazonCart, { ...mediumPizza, quantity: 2 });
  amazonCart = addToCart(amazonCart, { ...largePizza, quantity: 1 }, { ...amazonDiscount });

  const amazonDiscountPrice = amazonDiscount.price || 0;
  const expectedTotal = mediumPizza.price * 3 + amazonDiscountPrice;
  const totalCart = amazonCart.map((e) => e.total).reduce((a, b) => a + b, 0);
  expect(totalCart).toBe(expectedTotal);
});

test("amazon buy 3 medium + 3 large", () => {
  let amazonCart: CartInfo[] = [];
  const amazonDiscount: DiscountType = {
    kind: DiscountKind.price,
    productID: largePizza.id,
    price: 19.99,
  };
  const amazonDiscountPrice = amazonDiscount.price || 0;
  amazonCart = addToCart(amazonCart, { ...mediumPizza, quantity: 1 });
  amazonCart = addToCart(amazonCart, { ...mediumPizza, quantity: 2 });
  amazonCart = addToCart(amazonCart, { ...largePizza, quantity: 1 }, { ...amazonDiscount });
  amazonCart = addToCart(amazonCart, { ...largePizza, quantity: 2 }, { ...amazonDiscount });

  const expectedTotal = mediumPizza.price * 3 + amazonDiscountPrice * 3;
  const totalCart = amazonCart.map((e) => e.total).reduce((a, b) => a + b, 0);
  expect(totalCart).toBe(expectedTotal);
});

test("facebook buy", () => {
  let facebookCart: CartInfo[] = [];
  const facebookDiscount: DiscountType = {
    kind: DiscountKind.item,
    productID: mediumPizza.id,
    quantity: 4,
  };
  facebookCart = addToCart(facebookCart, { ...mediumPizza, quantity: 1 });
  facebookCart = addToCart(facebookCart, { ...mediumPizza, quantity: 4 }, { ...facebookDiscount });

  const expectedTotal = mediumPizza.price * (5 - 1);
  const totalCart = facebookCart.map((e) => e.total).reduce((a, b) => a + b, 0);
  expect(totalCart).toBe(expectedTotal);
});

test("facebook buy 10 medium", () => {
  let facebookCart: CartInfo[] = [];
  const facebookDiscount: DiscountType = {
    kind: DiscountKind.item,
    productID: mediumPizza.id,
    quantity: 4,
  };
  facebookCart = addToCart(facebookCart, { ...mediumPizza, quantity: 1 });
  facebookCart = addToCart(facebookCart, { ...mediumPizza, quantity: 9 }, { ...facebookDiscount });

  const expectedTotal = mediumPizza.price * (10 - 2);
  const totalCart = facebookCart.map((e) => e.total).reduce((a, b) => a + b, 0);
  expect(totalCart).toBe(expectedTotal);
});
