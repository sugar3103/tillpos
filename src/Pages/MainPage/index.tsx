import { useState } from "react";
import CartInfo from "../../types/cartInfo";
import DiscountType from "../../types/discount";
import { DiscountKind, ProductID, ProductKind, ProductsList } from "../../Utils/constant";
import { addToCart, storeCart } from "./cartFunction";
import "./index.scss";

function MainPage() {
  const cartInfoLocalStorage = localStorage.getItem("cartInfo");
  const parseCartInfo = cartInfoLocalStorage ? JSON.parse(cartInfoLocalStorage) : [];

  const [cartInfo, setCartInfo] = useState<CartInfo[]>(parseCartInfo);

  const cartTotal = cartInfo.map((e) => e.total).reduce((a, b) => a + b, 0);

  const buyNow = (itemToAdd: CartInfo) => {
    const newCartInfo = addToCart(cartInfo, { ...itemToAdd, quantity: 1 });
    setCartInfo(newCartInfo);
    storeCart(newCartInfo);
  };

  const smallPizza = ProductKind[ProductID.SMALL];
  const mediumPizza = ProductKind[ProductID.MEDIUM];
  const largePizza = ProductKind[ProductID.LARGE];

  const normalBuy = () => {
    let normalCart: CartInfo[] = [];
    normalCart = addToCart(normalCart, { ...smallPizza, quantity: 1 });
    normalCart = addToCart(normalCart, { ...mediumPizza, quantity: 1 });
    normalCart = addToCart(normalCart, { ...largePizza, quantity: 1 });
    setCartInfo(normalCart);
    storeCart(normalCart);
  };

  // thing to improve : rule can be added to every single buy

  const microsoftBuy = () => {
    let microsoftCart: CartInfo[] = [];
    const microsoftDiscount: DiscountType = {
      kind: DiscountKind.item,
      productID: smallPizza.id,
      quantity: 2,
    };
    microsoftCart = addToCart(microsoftCart, { ...smallPizza, quantity: 1 }, { ...microsoftDiscount });
    microsoftCart = addToCart(microsoftCart, { ...smallPizza, quantity: 2 }, { ...microsoftDiscount });
    microsoftCart = addToCart(microsoftCart, { ...largePizza, quantity: 1 });
    setCartInfo(microsoftCart);
    storeCart(microsoftCart);
  };

  const amazonBuy = () => {
    let amazonCart: CartInfo[] = [];
    const amazonDiscount: DiscountType = {
      kind: DiscountKind.price,
      productID: largePizza.id,
      price: 19.99,
    };
    amazonCart = addToCart(amazonCart, { ...mediumPizza, quantity: 1 });
    amazonCart = addToCart(amazonCart, { ...mediumPizza, quantity: 2 });
    amazonCart = addToCart(amazonCart, { ...largePizza, quantity: 1 }, { ...amazonDiscount });
    setCartInfo(amazonCart);
    storeCart(amazonCart);
  };

  const facebookBuy = () => {
    let facebookCart: CartInfo[] = [];
    const facebookDiscount: DiscountType = {
      kind: DiscountKind.item,
      productID: mediumPizza.id,
      quantity: 4,
    };
    facebookCart = addToCart(facebookCart, { ...mediumPizza, quantity: 1 });
    facebookCart = addToCart(facebookCart, { ...mediumPizza, quantity: 4 }, { ...facebookDiscount });
    setCartInfo(facebookCart);
    storeCart(facebookCart);
  };

  return (
    <div style={{ padding: 15 }}>
      <div className="mainPage">
        {/* left side panel to display product  */}
        <div style={{ padding: 4 }}>
          {ProductsList.map((e) => (
            <div key={`product_${e.id}`} className="product_wrap">
              <div style={{ width: 200, height: 150 }}>
                <img src={e.image} alt={"pizza " + e.image} width="100%" height="100%" />
              </div>
              <div style={{ textAlign: "left", padding: "10px" }}>
                <p style={{ marginBottom: 32 }}>
                  <span style={{ fontSize: 18, fontWeight: 900 }}>ID : </span>
                  <span>{e.id}</span>
                </p>
                <p>
                  <span style={{ fontSize: 18, fontWeight: 900 }}>Description: </span>
                  <span>{e.description}</span>
                </p>
                <div style={{ display: "flex", justifyContent: "space-between" }}>
                  <p style={{ fontWeight: 900, fontSize: 24 }}>{e.price}</p>
                  <button style={{ width: 150 }} onClick={() => buyNow({ ...e })}>
                    Buy now
                  </button>
                </div>
              </div>
            </div>
          ))}
        </div>
        {/* right side panel to display item in cart  */}
        <div className="" style={{ padding: 10, width: 500 }}>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <p style={{ fontWeight: 900, fontSize: 24 }}>Cart</p>
          </div>
          <div style={{ width: "100%" }}>
            {cartInfo.length === 0 && <p style={{ textAlign: "left" }}>No item in cart, please buy one</p>}
            {cartInfo?.map((eC: CartInfo) => (
              <div
                key={`cart_item_${eC.id}`}
                style={{ display: "flex", justifyContent: "space-between", marginBottom: 5 }}
              >
                <p style={{ fontSize: 18 }}>{eC.description}</p>
                <p style={{ fontWeight: 600, fontSize: 18 }}>
                  {eC.quantity}{" "}
                  {["+", "-"].map((e) => (
                    <span
                      key={`${eC.id}_${e}`}
                      className="increase_button"
                      onClick={() => {
                        const newCartInfo = addToCart(cartInfo, {
                          ...eC,
                          quantity: e === "+" ? 1 : -1,
                        });
                        setCartInfo(newCartInfo);
                        storeCart(newCartInfo);
                      }}
                    >
                      {e}
                    </span>
                  ))}
                </p>
                <p>{eC.price}$</p>
                <p>{Math.round(eC.total * 100) / 100}</p>
              </div>
            ))}
            <div
              style={{
                borderTop: "solid 1px black",
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <p style={{ fontSize: 20, fontWeight: 900 }}>Total</p>
              <p style={{ fontSize: 16 }}>{Math.round(cartTotal * 100) / 100}$</p>
            </div>
          </div>
        </div>
      </div>
      <div>
        <button className="customer_button" onClick={() => normalBuy()}>
          Normal Buy
        </button>
        <button className="customer_button" onClick={() => microsoftBuy()}>
          Microsoft Buy
        </button>
        <button className="customer_button" onClick={() => amazonBuy()}>
          Amazon Buy
        </button>
        <button className="customer_button" onClick={() => facebookBuy()}>
          Facebook buy
        </button>
      </div>
    </div>
  );
}

export default MainPage;
